﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameController : MonoBehaviour
{
    [SerializeField] private InventoryManager _inventoryManager;
    [SerializeField] private GameObject _blackScreen;
    [SerializeField] private GameObject _winScreen;
    [SerializeField] private Transform _backgroundsContainer;

    private Background _currentBackground;
    private bool _gameEnded = false;
    private List<Background> _backgrounds;
    private MoveArrow[] _moveArrows;
    private Dictionary<MoveDirections, MoveArrow> _movementArrowsOfDir;
    private Dictionary<BackgroundTypes, Background> _backgroundsOfType;

    #region MonoBehaviour

    private void Awake()
    {
        Assert.IsNotNull(_backgroundsContainer);
        Assert.IsNotNull(_inventoryManager);

        _backgrounds = new List<Background>();

        foreach (Transform child in _backgroundsContainer)
        {
            _backgrounds.Add(child.GetComponent<Background>());
        }

        _backgroundsOfType = new Dictionary<BackgroundTypes, Background>();

        for (int i = 0; i < _backgrounds.Count; i++)
        {
            if (_backgrounds[i] != null)
            {
                _backgroundsOfType.Add(_backgrounds[i].BackgroundType, _backgrounds[i]);
            }
        }

        _moveArrows = FindObjectsOfType<MoveArrow>();
        _movementArrowsOfDir = new Dictionary<MoveDirections, MoveArrow>();

        for (int i = 0; i < _moveArrows.Length; i++)
        {
            if (_moveArrows[i] != null)
            {
                _movementArrowsOfDir.Add(_moveArrows[i].Direction, _moveArrows[i]);
                _moveArrows[i].ArrowClicked += SetBackgroundTo;
            }
        }

        EventsManager.OnItemWithBackgroundClick += SetBackgroundTo;
        EventsManager.EndGame += OnGameEnd;
    }

    private void Start()
    {
        Initialize();
        _blackScreen.SetActive(false);
    }

    private void Update()
    {
        if (_gameEnded && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void Initialize()
    {
        for (int i = 0; i < _backgrounds.Count; i++)
        {
            _backgrounds[i].gameObject.SetActive(false);
        }

        SetBackgroundTo(BackgroundTypes.NorthWall);
    }

    private void OnGameEnd()
    {
        _backgroundsContainer.gameObject.SetActive(false);
        _winScreen.SetActive(true);
    }

    private void SetBackgroundTo(BackgroundTypes backgroundType)
    {
        if (!BackgroundCanBeShown(backgroundType))
        {
            return;
        }

        if (_currentBackground != null)
        {
            _currentBackground.gameObject.SetActive(false);
        }

        _currentBackground = _backgroundsOfType[backgroundType];

        SetupCurrentBackgroundArrows();

        _currentBackground.gameObject.SetActive(true);
    }

    private void SetupCurrentBackgroundArrows()
    {
        ResetArrowsSetup();

        for (int i = 0; i < _currentBackground.DirBackgrounds.Length; i++)
        {
            DirectionBackground currentDirBackground = _currentBackground.DirBackgrounds[i];

            MoveArrow arrowForBackground = _movementArrowsOfDir[currentDirBackground.MoveDirection];
            arrowForBackground.BackgroundToShow = currentDirBackground.BackgroundToShow;
        }
    }

    private void ResetArrowsSetup()
    {
        for (int i = 0; i < _moveArrows.Length; i++)
        {
            _moveArrows[i].BackgroundToShow = BackgroundTypes.None;
        }
    }

    private bool BackgroundCanBeShown(BackgroundTypes backgroundType)
    {
        if (backgroundType == BackgroundTypes.None)
        {
            Debug.LogError("It shouldn't be possible to proceed to background type of None.");
            return false;
        }
        else if (_currentBackground != null && _currentBackground.BackgroundType == backgroundType)
        {
            Debug.LogError("It shouldn't be possible to switch to the same background that is already active.");
            return false;
        }

        return true;
    }

    #endregion
}