﻿using UnityEngine;
using UnityEngine.UI;

public class Email : MonoBehaviour
{
    [SerializeField] private Button _sendButton;
    [SerializeField] private GameObject _emailSentScreen;
    [SerializeField] private InputField _toInputField;

    #region MonoBehaviour

    private void Awake()
    {
        _sendButton.onClick.AddListener(TrySendEmail);
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void TrySendEmail()
    {
        if (_toInputField.text == "lubiewgumie@punktgmail.com")
        {
            _emailSentScreen.SetActive(true);
            gameObject.SetActive(false);

            EventsManager.ClientMailSent();
        }
    }

    #endregion
}