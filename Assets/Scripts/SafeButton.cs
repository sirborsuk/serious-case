﻿using System;
using UnityEngine;

public class SafeButton : MonoBehaviour
{
    public Action<SafeButton> ButtonClicked;

    public char[] ButtonLetters;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        ButtonClicked?.Invoke(this);
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}