﻿using UnityEngine;

public class Puzzles : MonoBehaviour
{
    [SerializeField] private GameObject _openedPuzzles;

    private PuzzlePiece _selectedPiece;
    private PuzzlePiece[,] _puzzlePieces;
    private int _rows = 4;
    private int _columns = 4;

    #region MonoBehaviour

    private void Awake()
    {
        _puzzlePieces = new PuzzlePiece[_rows, _columns];

        for (int rowIndex = 0; rowIndex < _rows; rowIndex++)
        {
            for (int columnIndex = 0; columnIndex < _columns; columnIndex++)
            {
                Transform currentChild = transform.GetChild(rowIndex * _rows + columnIndex);

                PuzzlePiece currentPiece = currentChild.GetComponent<PuzzlePiece>();
                _puzzlePieces[currentPiece.CurrentRow, currentPiece.CurrentColumn] = currentPiece;
                currentPiece.PieceSelected += OnPuzzleSelected;
                currentPiece.PieceDeselected += OnPuzzleDeselected;
                currentPiece.PieceSwiped += OnPuzzleSwiped;
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnPuzzleSelected(PuzzlePiece piece)
    {
        _selectedPiece = piece;
    }

    private void OnPuzzleDeselected()
    {
        _selectedPiece = null;
    }

    private void OnPuzzleSwiped(SwipeDirection swipeTo)
    {
        if (_selectedPiece != null)
        {
            switch (swipeTo)
            {
                case SwipeDirection.Right:
                    if (_selectedPiece.CurrentColumn < _columns - 1)
                    {
                        PuzzlePiece swipedPiece = _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn + 1];

                        if (!swipedPiece.IsEmptyPiece)
                        {
                            break;
                        }

                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn + 1] = _selectedPiece;
                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn] = swipedPiece;

                        swipedPiece.CurrentColumn--;
                        _selectedPiece.CurrentColumn++;

                        Vector3 swipedPiecePos = swipedPiece.transform.position;
                        swipedPiece.transform.position = _selectedPiece.transform.position;
                        _selectedPiece.transform.position = swipedPiecePos;
                    }
                    break;
                case SwipeDirection.Down:
                    if (_selectedPiece.CurrentRow < _rows - 1)
                    {
                        PuzzlePiece swipedPiece = _puzzlePieces[_selectedPiece.CurrentRow + 1, _selectedPiece.CurrentColumn];

                        if (!swipedPiece.IsEmptyPiece)
                        {
                            break;
                        }

                        _puzzlePieces[_selectedPiece.CurrentRow + 1, _selectedPiece.CurrentColumn] = _selectedPiece;
                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn] = swipedPiece;

                        swipedPiece.CurrentRow--;
                        _selectedPiece.CurrentRow++;

                        Vector3 swipedPiecePos = swipedPiece.transform.position;
                        swipedPiece.transform.position = _selectedPiece.transform.position;
                        _selectedPiece.transform.position = swipedPiecePos;
                    }
                    break;
                case SwipeDirection.Left:
                    if (_selectedPiece.CurrentColumn > 0)
                    {
                        PuzzlePiece swipedPiece = _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn - 1];

                        if (!swipedPiece.IsEmptyPiece)
                        {
                            break;
                        }

                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn - 1] = _selectedPiece;
                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn] = swipedPiece;

                        swipedPiece.CurrentColumn++;
                        _selectedPiece.CurrentColumn--;

                        Vector3 swipedPiecePos = swipedPiece.transform.position;
                        swipedPiece.transform.position = _selectedPiece.transform.position;
                        _selectedPiece.transform.position = swipedPiecePos;
                    }
                    break;
                case SwipeDirection.Up:
                    if (_selectedPiece.CurrentRow > 0)
                    {
                        PuzzlePiece swipedPiece = _puzzlePieces[_selectedPiece.CurrentRow - 1, _selectedPiece.CurrentColumn];

                        if (!swipedPiece.IsEmptyPiece)
                        {
                            break;
                        }

                        _puzzlePieces[_selectedPiece.CurrentRow - 1, _selectedPiece.CurrentColumn] = _selectedPiece;
                        _puzzlePieces[_selectedPiece.CurrentRow, _selectedPiece.CurrentColumn] = swipedPiece;

                        swipedPiece.CurrentRow++;
                        _selectedPiece.CurrentRow--;

                        Vector3 swipedPiecePos = swipedPiece.transform.position;
                        swipedPiece.transform.position = _selectedPiece.transform.position;
                        _selectedPiece.transform.position = swipedPiecePos;
                    }
                    break;
                default:
                    break;
            }
        }
        
        _selectedPiece = null;

        if (PuzzlesSolved())
        {
            _openedPuzzles.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    private bool PuzzlesSolved()
    {
        foreach (PuzzlePiece piece in _puzzlePieces)
        {
            if (piece.CurrentColumn != piece.RealColumn || piece.CurrentRow != piece.RealRow)
            {
                return false;
            }
        }

        return true;
    }

    #endregion
}