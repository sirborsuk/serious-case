﻿using UnityEngine;
using UnityEngine.UI;

public class LoginScreen : MonoBehaviour
{
    [SerializeField] private GameObject _emailScreen;
    [SerializeField] private Slider _rSlider;
    [SerializeField] private Slider _gSlider;
    [SerializeField] private Slider _bSlider;
    [SerializeField] private Text _rText;
    [SerializeField] private Text _gText;
    [SerializeField] private Text _bText;

    #region MonoBehaviour

    private void Awake()
    {
        _rSlider.onValueChanged.AddListener(OnRedValueChanged);
        _gSlider.onValueChanged.AddListener(OnGreenValueChanged);
        _bSlider.onValueChanged.AddListener(OnBlueValueChanged);
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnRedValueChanged(float value)
    {
        _rText.text = Mathf.Round(value).ToString();
        CheckIfValueReached();
    }

    private void OnGreenValueChanged(float value)
    {
        _gText.text = Mathf.Round(value).ToString();
        CheckIfValueReached();
    }

    private void OnBlueValueChanged(float value)
    {
        _bText.text = Mathf.Round(value).ToString();
        CheckIfValueReached();
    }

    private void CheckIfValueReached()
    {
        if (Mathf.Round(_rSlider.value) == 250 && Mathf.Round(_gSlider.value) == 150 && Mathf.Round(_bSlider.value) == 200)
        {
            EventsManager.RixQuestComplete();
            _emailScreen.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    #endregion
}