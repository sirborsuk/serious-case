﻿using System;
using UnityEngine;

public enum SwipeDirection
{
    Right = 1,
    Down = 2,
    Left = 3,
    Up = 4
}

public class PuzzlePiece : MonoBehaviour
{
    public Action PieceDeselected;
    public Action<PuzzlePiece> PieceSelected;
    public Action<SwipeDirection> PieceSwiped;

    public bool IsEmptyPiece;
    public int CurrentRow;
    public int CurrentColumn;
    public int RealRow;
    public int RealColumn;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        PieceSelected?.Invoke(this);
    }

    private void OnMouseUp()
    {        
        PieceDeselected?.Invoke();
    }

    private void OnMouseExit()
    {
        Vector3 swipeDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        SwipeDirection swipeTo = SwipeDirection.Right;

        if (Mathf.Abs(swipeDirection.x) > Mathf.Abs(swipeDirection.y))
        {
            if (swipeDirection.x < 0)
            {
                swipeTo = SwipeDirection.Left;
            }
        }
        else
        {
            if (swipeDirection.y > 0)
            {
                swipeTo = SwipeDirection.Up;
            }
            else
            {
                swipeTo = SwipeDirection.Down;
            }
        }

        PieceSwiped?.Invoke(swipeTo);
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}