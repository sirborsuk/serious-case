﻿using UnityEngine;

public class SpriteSwapAtItemPickup : MonoBehaviour
{
    [SerializeField] private ItemTypes _itemToPickup;
    [SerializeField] private SpriteRenderer _srenderer;
    [SerializeField] private Sprite _newSprite;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.OnItemToPickupClick += OnItemPickedUp;
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnItemPickedUp(ItemTypes itemType)
    {
        if (_itemToPickup == itemType)
        {
            _srenderer.sprite = _newSprite;
        }
    }

    #endregion
}