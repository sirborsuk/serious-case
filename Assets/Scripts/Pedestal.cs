﻿using UnityEngine;

public class Pedestal : MonoBehaviour
{
    [SerializeField] private GameObject _adamTear;
    [SerializeField] private GameObject _adamTearZoomed;
    [SerializeField] private GameObject _rixPride;
    [SerializeField] private GameObject _rixPrideZoomed;
    [SerializeField] private GameObject _clientPleasure;
    [SerializeField] private GameObject _clientPleasureZoomed;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        if (InventoryManager.SelectedItem != null && InventoryManager.SelectedItem.ItemType == ItemTypes.AdamTear)
        {
            _adamTear.SetActive(true);
            _adamTearZoomed.SetActive(true);
            EventsManager.OnItemUsed(ItemTypes.AdamTear);
        }
        else if (InventoryManager.SelectedItem != null && InventoryManager.SelectedItem.ItemType == ItemTypes.RixPride)
        {
            _rixPride.SetActive(true);
            _rixPrideZoomed.SetActive(true);
            EventsManager.OnItemUsed(ItemTypes.RixPride);
        }
        else if (InventoryManager.SelectedItem != null && InventoryManager.SelectedItem.ItemType == ItemTypes.ClientPleasure)
        {
            _clientPleasure.SetActive(true);
            _clientPleasureZoomed.SetActive(true);
            EventsManager.OnItemUsed(ItemTypes.ClientPleasure);
        }

        if (_adamTearZoomed.activeInHierarchy && _rixPrideZoomed.activeInHierarchy && _clientPleasureZoomed.activeInHierarchy)
        {
            EventsManager.PedestalFilled();
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}