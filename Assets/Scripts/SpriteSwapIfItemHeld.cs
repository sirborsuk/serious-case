﻿using UnityEngine;

public class SpriteSwapIfItemHeld : MonoBehaviour
{
    [SerializeField] private bool _swapBackPossible;
    [SerializeField] private GameObject _itemToShowAtSwap;
    [SerializeField] private ItemTypes _itemTypeTrigger;
    [SerializeField] private Sprite _newSprite;

    private SpriteRenderer _srend;

    #region MonoBehaviour

    private void Awake()
    {
        _srend = GetComponent<SpriteRenderer>();
    }

    private void OnMouseDown()
    {
        if (InventoryManager.ItemIsSelected(_itemTypeTrigger))
        {
            if (_itemToShowAtSwap != null)
            {
                _itemToShowAtSwap.SetActive(true);
            }

            EventsManager.OnItemUsed(_itemTypeTrigger);
            Sprite oldSprite = _srend.sprite;
            _srend.sprite = _newSprite;

            if (_swapBackPossible)
            {
                _newSprite = oldSprite;
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    #endregion
}