﻿using UnityEngine;

public class SpriteSwapAtItemUsed : MonoBehaviour
{
    [SerializeField] private bool _swapBackPossible;
    [SerializeField] private GameObject _itemToShowAtSwap;
    [SerializeField] private ItemTypes _itemTypeTrigger;
    [SerializeField] private Sprite _newSprite;

    private SpriteRenderer _srend;

    #region MonoBehaviour

    private void Awake()
    {
        _srend = GetComponent<SpriteRenderer>();

        EventsManager.OnItemUsed += OnItemUsed;
    }

    private void OnMouseDown()
    {
        if (_itemTypeTrigger == ItemTypes.None)
        {
            Sprite oldSprite = _srend.sprite;
            _srend.sprite = _newSprite;

            if (_itemToShowAtSwap != null)
            {
                _itemToShowAtSwap.SetActive(true);
            }

            if (_swapBackPossible)
            {
                _newSprite = oldSprite;
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnItemUsed(ItemTypes itemType)
    {
        if (itemType == _itemTypeTrigger)
        {
            _srend.sprite = _newSprite;
        }
    }

    #endregion
}