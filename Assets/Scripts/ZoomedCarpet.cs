﻿using UnityEngine;
using UnityEngine.Assertions;

public class ZoomedCarpet : MonoBehaviour
{
    [SerializeField] private GameObject _carpetUncovered;
    [SerializeField] private GameObject _carpetWoodRemoved;

    private Collider2D _collider2D;

    #region MonoBehaviour

    private void Awake()
    {
        Assert.IsNotNull(_carpetUncovered);
        Assert.IsNotNull(_carpetWoodRemoved);

        _collider2D = GetComponent<Collider2D>();
        Assert.IsNotNull(_collider2D);
    }

    private void OnMouseDown()
    {
        if (!_carpetWoodRemoved.activeInHierarchy)
        {
            if (!_carpetUncovered.activeInHierarchy)
            {
                _carpetUncovered.SetActive(true);
            }
            else if (InventoryManager.SelectedItem != null && InventoryManager.SelectedItem.ItemType == ItemTypes.ChairLeg)
            {
                EventsManager.OnItemUsed(ItemTypes.ChairLeg);
                _collider2D.enabled = false;
                _carpetWoodRemoved.SetActive(true);
            }
            else
            {
                _carpetUncovered.SetActive(false);
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}