﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Safe : MonoBehaviour
{
    private const string PASSWORD = "MASŁO";

    [SerializeField] private GameObject _openSafe;
    [SerializeField] private Text _passwordText;

    private bool _swapLetter = false;
    private float _timeToAcceptLetter = 1.5f;
    private SafeButton[] _safeButtons;
    private SafeButton _lastButtonClicked;

    #region MonoBehaviour

    private void Awake()
    {
        _safeButtons = GetComponentsInChildren<SafeButton>();

        for (int i = 0; i < _safeButtons.Length; i++)
        {
            _safeButtons[i].ButtonClicked += OnButtonClicked;
        }
    }

    private void OnEnable()
    {
        _swapLetter = false;
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnButtonClicked(SafeButton safeButton)
    {
        StopAllCoroutines();

        if (_lastButtonClicked != null && _lastButtonClicked == safeButton && _swapLetter)
        {
            char passwordLastLetter = _passwordText.text[_passwordText.text.Length - 1];
            int indexOfNextLetter = 0;

            for (int i = 0; i < safeButton.ButtonLetters.Length; i++)
            {
                if (safeButton.ButtonLetters[i] == passwordLastLetter)
                {
                    indexOfNextLetter = i + 1;
                    break;
                }
            }

            indexOfNextLetter = indexOfNextLetter % safeButton.ButtonLetters.Length;

            _passwordText.text = _passwordText.text.Remove(_passwordText.text.Length - 1);
            _passwordText.text += safeButton.ButtonLetters[indexOfNextLetter];
        }
        else
        {
            _passwordText.text += safeButton.ButtonLetters[0];
        }

        if (_passwordText.text.Length < PASSWORD.Length)
        {
            StartCoroutine(SwapLetterCoroutine());
            _lastButtonClicked = safeButton;
        }
        else
        {
            if (_passwordText.text == PASSWORD)
            {
                _openSafe.SetActive(true);
                enabled = false;
                _passwordText.gameObject.SetActive(false);

                for (int i = 0; i < _safeButtons.Length; i++)
                {
                    _safeButtons[i].gameObject.SetActive(false);
                }
            }
            else
            {
                _passwordText.text = "";
            }
        }
    }

    private IEnumerator SwapLetterCoroutine()
    {
        _swapLetter = true;
        yield return new WaitForSeconds(_timeToAcceptLetter);
        _swapLetter = false;
    }

    #endregion
}