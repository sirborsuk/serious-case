﻿using UnityEngine;

public class Client : MonoBehaviour
{
    [SerializeField] private AudioClip _clientMail;
    [SerializeField] private AudioClip _clientWin;
    [SerializeField] private AudioSource _audioSource;

    private bool _winPlayed = false;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.RixQuestComplete += OnRixQuestComplete;
        EventsManager.ClientMailSent += OnClientMailSent;
    }

    private void OnMouseDown()
    {
        if (_audioSource.clip != null && !_audioSource.isPlaying && !_winPlayed)
        {
            _audioSource.Play();

            if (_audioSource.clip == _clientWin)
            {
                _winPlayed = true;
                EventsManager.OnItemToPickupClick(ItemTypes.ClientPleasure);
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnRixQuestComplete()
    {
        _audioSource.clip = _clientMail;
    }

    private void OnClientMailSent()
    {
        _audioSource.clip = _clientWin;
    }

    #endregion
}