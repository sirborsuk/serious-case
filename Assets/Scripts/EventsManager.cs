﻿using System;
using UnityEngine;

public class EventsManager : MonoBehaviour
{
    public static Action EndGame;
    public static Action PedestalFilled;
    public static Action AdamMoustacheShaved;
    public static Action RixQuestComplete;
    public static Action ClientMailSent;
    public static Action<BackgroundTypes> OnItemWithBackgroundClick;
    public static Action<ItemTypes> OnItemDiscarded;
    public static Action<ItemTypes> OnItemToPickupClick;
    public static Action<ItemTypes> OnItemUsed;

    #region MonoBehaviour



    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}