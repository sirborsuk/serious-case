﻿using System;

[Serializable]
public struct DirectionBackground
{
    public MoveDirections MoveDirection;
    public BackgroundTypes BackgroundToShow;
}