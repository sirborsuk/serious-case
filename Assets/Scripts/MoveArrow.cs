﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public enum MoveDirections
{
    Right = 0,
    Back = 1,
    Left = 2
}

public class MoveArrow : MonoBehaviour
{
    public Action<BackgroundTypes> ArrowClicked;

    public BackgroundTypes BackgroundToShow
    {
        get => _backgroundToShow;
        set
        {
            if (value == BackgroundTypes.None)
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
            }

            _backgroundToShow = value;
        }
    }

    public MoveDirections Direction
    {
        get => _direction;
        private set => _direction = value;
    }

    [SerializeField] private BackgroundTypes _backgroundToShow;
    [SerializeField] private MoveDirections _direction;

    private Button _arrowButton;

    #region MonoBehaviour

    private void Awake()
    {
        _arrowButton = GetComponent<Button>();
        Assert.IsNotNull(_arrowButton);
    }

    private void Start()
    {
        _arrowButton.onClick.AddListener(OnArrowClicked);
    }

    #endregion

    #region Public Interface

    #endregion

    #region Private Methods

    private void OnArrowClicked()
    {
        ArrowClicked?.Invoke(BackgroundToShow);
    }

    #endregion
}
