﻿using UnityEngine;

public class Doors : MonoBehaviour
{
    private bool _pedestalFilled = false;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.PedestalFilled += OnPedestalFilled;
    }

    private void OnMouseDown()
    {
        if (_pedestalFilled)
        {
            EventsManager.EndGame();
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnPedestalFilled()
    {
        _pedestalFilled = true;
    }

    #endregion
}