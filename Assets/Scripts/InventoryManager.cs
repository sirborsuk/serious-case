﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public static InventoryItem SelectedItem
    {
        get => _selectedItem;
        private set => _selectedItem = value;
    }

    [SerializeField] private IconsLib _iconsLib;
    [SerializeField] private InventoryItem _itemTemplate;

    private Dictionary<ItemTypes, ItemTypes> _mergeOptions;
    private Dictionary<ItemTypes, ItemTypes> _mergeResults;
    private Dictionary<ItemTypes, InventoryItem> _inventoryItems;
    private static InventoryItem _selectedItem;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.OnItemUsed += RemoveItem;
        EventsManager.OnItemToPickupClick += GainItem;

        _mergeOptions = new Dictionary<ItemTypes, ItemTypes>();
        _mergeOptions.Add(ItemTypes.Razor, ItemTypes.RazorBlade);
        _mergeOptions.Add(ItemTypes.RazorBlade, ItemTypes.Razor);

        _mergeResults = new Dictionary<ItemTypes, ItemTypes>();
        _mergeResults.Add(ItemTypes.Razor, ItemTypes.RazorWithBlade);
        _mergeResults.Add(ItemTypes.RazorBlade, ItemTypes.RazorWithBlade);

        _inventoryItems = new Dictionary<ItemTypes, InventoryItem>();
    }

    #endregion

    #region Public Interface

    public static bool ItemIsSelected(ItemTypes itemType)
    {
        if (SelectedItem != null && SelectedItem.ItemType == itemType)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void GainItem(ItemTypes itemType)
    {
        if (itemType == ItemTypes.None)
        {
            Debug.LogError("It shouldn't be possible to add a None item to the inventory!");
            return;
        }

        InventoryItem newItem = Instantiate(_itemTemplate, transform);
        newItem.ItemClicked += OnItemClicked;
        newItem.SetupItem(itemType, _iconsLib.Icons[(int)itemType]);
        _inventoryItems.Add(itemType, newItem);
    }

    public void RemoveItem(ItemTypes itemType)
    {
        if (_inventoryItems.ContainsKey(itemType))
        {
            InventoryItem itemToRemove = _inventoryItems[itemType];
            _inventoryItems.Remove(itemToRemove.ItemType);
            Destroy(itemToRemove.gameObject);
        }
    }

    #endregion

    #region Private Methods

    private void OnItemClicked(InventoryItem item)
    {
        if (SelectedItem == null)
        {
            SelectedItem = item;
            item.SetSelected(true);
        }
        else
        {
            if (SelectedItem == item)
            {
                SelectedItem.SetSelected(false);
                SelectedItem = null;
            }
            else if (ItemsCanBeMerged(SelectedItem.ItemType, item.ItemType))
            {
                MergeItems(SelectedItem, item);
            }
            else
            {
                SelectedItem.SetSelected(false);
                SelectedItem = null;
            }
        }
    }

    private void MergeItems(InventoryItem firstItem, InventoryItem secondItem)
    {
        GainItem(_mergeResults[firstItem.ItemType]);
        RemoveItem(firstItem.ItemType);
        RemoveItem(secondItem.ItemType);
        _selectedItem = null;
    }

    private bool ItemsCanBeMerged(ItemTypes firstItemType, ItemTypes secondItemType)
    {
        if (_mergeOptions.ContainsKey(firstItemType) && _mergeOptions[firstItemType] == secondItemType)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion
}