﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public enum ItemTypes
{
    None = 0,
    ChairLeg = 1,
    Razor = 2,
    RazorBlade = 3,
    RazorWithBlade = 4,
    GoldenKey = 5,
    AdamTear = 6,
    RixPride = 7,
    ClientPleasure = 8,
    Bolt = 9,
    Wrench = 10,
    Moustache = 11
}

public class InventoryItem : MonoBehaviour
{
    public Action<InventoryItem> ItemClicked;

    public ItemTypes ItemType
    {
        get => _itemType;
        private set => _itemType = value;
    }

    [SerializeField] private Color _selectedColor;
    [SerializeField] private Color _unselectedColor;
    [SerializeField] private Image _itemImage;

    private bool _isSelected;
    private Button _button;
    private Image _itemBackground;
    private ItemTypes _itemType;

    #region MonoBehaviour

    private void Awake()
    {
        _button = GetComponent<Button>();
        Assert.IsNotNull(_button);

        _button.onClick.AddListener(OnItemClicked);

        _itemBackground = GetComponent<Image>();
        Assert.IsNotNull(_itemBackground);

        Assert.IsNotNull(_itemImage);
    }

    #endregion

    #region Public Interface

    public void SetupItem(ItemTypes itemType, Sprite icon)
    {
        ItemType = itemType;
        _itemImage.sprite = icon;
    }

    public void SetSelected(bool isSelected)
    {
        _isSelected = isSelected;

        if (_isSelected)
        {
            _itemBackground.color = _selectedColor;
        }
        else
        {
            _itemBackground.color = _unselectedColor;
        }
    }

    #endregion

    #region Private Methods

    private void OnItemClicked()
    {
        ItemClicked?.Invoke(this);
    }

    #endregion
}