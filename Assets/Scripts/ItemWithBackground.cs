﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ItemWithBackground : MonoBehaviour
{
    [SerializeField] private BackgroundTypes _backgroundToShow;

    #region MonoBehaviour

    private void Awake()
    {
        if (_backgroundToShow == BackgroundTypes.None)
        {
            Debug.LogError("Item with background should have a background to show.");
        }
    }

    public void OnMouseDown()
    {
        EventsManager.OnItemWithBackgroundClick(_backgroundToShow);
    }

	#endregion

	#region Public Interface



	#endregion

	#region Private Methods



	#endregion
}