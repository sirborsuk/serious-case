﻿using UnityEngine;
using UnityEngine.Assertions;

public enum BackgroundTypes
{
    None = 0,
    NorthWall = 1,
    EastWall = 2,
    SouthWall = 3,
    WestWall = 4,
    Pedestal = 5,
    Carpet = 6,
    Computer = 7,
    Rix = 8,
    Chair = 9,
    Drawer = 10,
    Adam = 11,
    Painting = 12,
    Client = 13,
    Cupboard = 14,
    Safe = 15,
    Puzzle = 16
}

public class Background : MonoBehaviour
{
    public BackgroundTypes BackgroundType
    {
        get => _backgroundType;
        private set => _backgroundType = value;
    }

    public DirectionBackground[] DirBackgrounds
    {
        get => _dirBackgrounds;
        set => _dirBackgrounds = value;
    }

    [SerializeField] private BackgroundTypes _backgroundType;
    [SerializeField] private DirectionBackground[] _dirBackgrounds;

    private SpriteRenderer _srend;

    private void Awake()
    {
        _srend = GetComponent<SpriteRenderer>();
        Assert.IsNotNull(_srend);
    }
}