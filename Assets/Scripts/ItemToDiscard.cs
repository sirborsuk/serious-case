﻿using UnityEngine;

public class ItemToDiscard : MonoBehaviour
{
    [SerializeField] private ItemTypes _itemType;
    [SerializeField] private ItemTypes _itemRequired;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        if (_itemRequired == ItemTypes.None)
        {
            EventsManager.OnItemDiscarded(_itemType);
            gameObject.SetActive(false);
        }
        else if (InventoryManager.ItemIsSelected(_itemRequired))
        {
            EventsManager.OnItemUsed(_itemRequired);
            EventsManager.OnItemDiscarded(_itemType);
            gameObject.SetActive(false);
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}