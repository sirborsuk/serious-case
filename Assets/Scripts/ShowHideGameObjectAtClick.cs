﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowHideGameObjectAtClick : MonoBehaviour
{
    [SerializeField] private GameObject _gameObjectToShow;
    [SerializeField] private GameObject _gameObjectToHide;
    [SerializeField] private ItemTypes _requiredItemType;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        if (_requiredItemType == ItemTypes.None || InventoryManager.ItemIsSelected(_requiredItemType))
        {
            if (_requiredItemType != ItemTypes.None)
            {
                EventsManager.OnItemUsed(_requiredItemType);
            }

            if (_gameObjectToShow != null)
            {
                _gameObjectToShow.SetActive(true);
            }

            if (_gameObjectToHide != null)
            {
                _gameObjectToHide.SetActive(false);
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}