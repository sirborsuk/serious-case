﻿using UnityEngine;

public class ItemToPickup : MonoBehaviour
{
    [SerializeField] private ItemTypes _itemType;
    [SerializeField] private ItemTypes _itemRequired;
    [SerializeField] private ItemTypes _itemToDiscardBefore;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.OnItemDiscarded += CheckIfProperItemDiscarded;
    }

    private void OnMouseDown()
    {
        if (_itemToDiscardBefore == ItemTypes.None)
        {
            if (_itemRequired == ItemTypes.None)
            {
                EventsManager.OnItemToPickupClick(_itemType);
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
            else if (InventoryManager.ItemIsSelected(_itemRequired))
            {
                EventsManager.OnItemUsed(_itemRequired);
                EventsManager.OnItemToPickupClick(_itemType);
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void CheckIfProperItemDiscarded(ItemTypes itemType)
    {
        if (_itemToDiscardBefore == itemType)
        {
            _itemToDiscardBefore = ItemTypes.None;
        }
    }

    #endregion
}