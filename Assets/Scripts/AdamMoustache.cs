﻿using UnityEngine;

public class AdamMoustache : MonoBehaviour
{
    [SerializeField] AudioClip[] _barkClips;
    [SerializeField] AudioClip _cryClip;
    [SerializeField] AudioSource _audioSource;
    [SerializeField] GameObject _tear;

    private bool _moustacheShaved = false;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.OnItemDiscarded += OnItemDiscarded;
    }

    private void OnMouseDown()
    {
        if (!_moustacheShaved && !_audioSource.isPlaying)
        {
            int randomBarkIndex = Random.Range(0, _barkClips.Length);
            AudioClip randomBark = _barkClips[randomBarkIndex];

            _audioSource.clip = randomBark;
            _audioSource.Play();
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnItemDiscarded(ItemTypes itemType)
    {
        if (itemType == ItemTypes.Moustache)
        {
            _moustacheShaved = true;
            _audioSource.clip = _cryClip;
            _audioSource.Play();
            _tear.SetActive(true);
        }
    }

    #endregion
}