﻿using UnityEngine;

public class Rix : MonoBehaviour
{
    [SerializeField] private AudioClip _rixWin;
    [SerializeField] private AudioSource _audioSource;

    private bool _winPlayed = false;

    #region MonoBehaviour

    private void Awake()
    {
        EventsManager.RixQuestComplete += OnQuestComplete;
    }

    private void OnMouseDown()
    {
        if (_audioSource.clip != null && !_audioSource.isPlaying && !_winPlayed)
        {
            _audioSource.Play();

            if (_audioSource.clip == _rixWin)
            {
                _winPlayed = true;
                EventsManager.OnItemToPickupClick(ItemTypes.RixPride);
            }
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods

    private void OnQuestComplete()
    {
        _audioSource.clip = _rixWin;
    }

    #endregion
}