﻿using UnityEngine;

public class ChangeGameobjectAtClick : MonoBehaviour
{
    [SerializeField] private GameObject _gameObjectToShow;
    [SerializeField] private ItemTypes _requiredItemType;

    #region MonoBehaviour

    private void OnMouseDown()
    {
        if (_requiredItemType == ItemTypes.None || InventoryManager.ItemIsSelected(_requiredItemType))
        {
            if (_requiredItemType != ItemTypes.None)
            {
                EventsManager.OnItemUsed(_requiredItemType);
            }

            _gameObjectToShow.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    #endregion

    #region Public Interface



    #endregion

    #region Private Methods



    #endregion
}